utils: bdaddr: Add service to set Bluetooth device (MAC) address
================================================================

The Bluetooth chip does not come with a unique Bluetooth device (MAC)
address configured out of the box. This means that we manually need
to configure the device address.

Otherwise BT will stop working from v6.5 kernel version onwards.
Reference: https://bugs.linaro.org/show_bug.cgi?id=5998

Link: https://github.com/me176c-dev/android_device_asus_K013/commit/cbb7066

Original source code is from AOSP: https://android.googlesource.com/device/linaro/dragonboard/+/refs/heads/main/shared/utils/bdaddr/
